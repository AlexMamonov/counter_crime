from rest_framework import serializers
from weapon_room.models import WeaponRoom

class WeaponRoomSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = WeaponRoom
        fields = '__all__'
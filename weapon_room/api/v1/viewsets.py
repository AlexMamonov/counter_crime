from .serializers import WeaponRoomSerializer
from weapon_room.models import WeaponRoom
from user.permissions import ExtendedModelPermissionsNonAuthReadOnly
from rest_framework.filters import SearchFilter
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import (
    generics,
    permissions
)


class WeaponRoomList(generics.ListCreateAPIView):
    queryset = WeaponRoom.objects.all()
    serializer_class = WeaponRoomSerializer

    filter_backends = (
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter,
    )

    filter_fields = '__all__'
    search_fields = '__all__'
    ordering_fields = '__all__'

    permission_classes = [
        # permissions.IsAuthenticatedOrReadOnly,
        ExtendedModelPermissionsNonAuthReadOnly
        # permissions.DjangoModelPermissionsOrAnonReadOnly,
        # ExtendedModelPermissions
    ]


class WeaponRoomDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = WeaponRoom.objects.all()
    serializer_class = WeaponRoomSerializer

    permission_classes = [
        # permissions.IsAuthenticated,
        # permissions.IsAuthenticatedOrReadOnly,
        ExtendedModelPermissionsNonAuthReadOnly
        # ExtendedModelPermissions
    ]
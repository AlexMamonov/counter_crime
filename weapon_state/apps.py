from django.apps import AppConfig


class WeaponStateConfig(AppConfig):
    name = 'weapon_state'

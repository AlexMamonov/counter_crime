from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import  (WeaponStateList, WeaponStateDetail)

urlpatterns = [
    url(r'^weapon_states/$', WeaponStateList.as_view(), name='weaponstate-list'),
    url(r'^weapon_states/(?P<pk>[0-9]+)$', WeaponStateDetail.as_view(), name='weaponstate-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)

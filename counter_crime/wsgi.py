"""
WSGI config for counter_crime project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
from utils.transaction_worker import TransationWorker

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'counter_crime.settings')

transation_worker = TransationWorker()

transation_worker.start()

application = get_wsgi_application()

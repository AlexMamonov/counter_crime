import threading
from utils.singleton import SingletonType
from active_transaction.models import ActiveTransaction
from completed_transaction.models import CompletedTransaction
from weapon_room.models import WeaponRoom
from counter_crime.settings import TRANSACTION_PROCESSING_TIMEOUT
import time
from pytz import timezone
from datetime import datetime, timedelta
from utils.helpers import convert_minues_to_seconds


class TransationWorker(threading.Thread, metaclass=SingletonType):

    def __init__(self):
        threading.Thread.__init__(self)
        self.name = "TransationManager"

    def run(self):
        print("Starting" + self.name)
        self.transaction_processing()

    def transaction_processing(self):
        while True:
            time.sleep(convert_minues_to_seconds(
                TRANSACTION_PROCESSING_TIMEOUT))
            # time.sleep(10)
            active_transaction = ActiveTransaction.objects.all()
            for transaction in active_transaction:
                if transaction.created < (datetime.now(timezone('Europe/Moscow')) - timedelta(
                    hours=transaction.expire_time.hour,
                    minutes=transaction.expire_time.minute, 
                    seconds=transaction.expire_time.second
                )):
                    completed_transacton = CompletedTransaction.objects.create(
                        client=transaction.client,
                        weapon=transaction.weapon
                    )
                    completed_transacton.save()
                    transaction.tier.active_work_period.completed_transaction.add(completed_transacton)
                    transaction.tier.active_work_period.active_transaction.remove(transaction)
                    transaction.delete()
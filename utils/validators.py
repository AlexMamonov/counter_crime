from django.core.validators import BaseValidator, RegexValidator
from django.utils.deconstruct import deconstructible
from datetime import date
from counter_crime.settings import MINIMAL_AGE
from utils.get_year import get_year

@deconstructible
class DepartamentCodeValidator(RegexValidator):
    regex = '^[0-9]{3}-[0-9]{3}$'
    message = "Код подразделения должен быть в формате [012-345]"
    code = 'nomatch'

@deconstructible
class NameValidator(RegexValidator):
    regex = '^[А-Я][а-я]+'
    message = "Поле должно быть в формате [Абвгд]"
    code = 'nomatch'

@deconstructible
class BirthDateValidator(BaseValidator):
    def __init__(self, message="Дата не соответсвует минимальному возрасту (минимальный возраст = {})".format(MINIMAL_AGE)):
        super().__init__(limit_value=None, message=message)

    def compare(self, a, b):
        age = get_year((date.today() - a))
        return  age < MINIMAL_AGE

@deconstructible
class PhoneValidator(RegexValidator):
    regex = '^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{10,10}$'
    message = "Не корректный номер телефона"
    code = 'nomatch'

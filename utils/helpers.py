import re

def convert_minues_to_seconds(minutes):
    return minutes * 60

def check_phone_number(phone_number):
    p = re.compile('^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{10,10}$')
    return bool(p.match(phone_number))

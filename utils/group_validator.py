from django.core.validators import BaseValidator
from django.utils.deconstruct import deconstructible
from django.contrib.auth.models import Group
from user.models import User
from django.core.exceptions import ValidationError
from counter_crime.settings import PERMISSION_CLASSES

@deconstructible
class StaffValidator(BaseValidator):
    def __init__(self, message="Клиенты не могут быть сотрудниками"):
        super().__init__(limit_value=None, message=message)

    def __call__(self, value):
        if isinstance(value, int):
            user = User.objects.get(pk=value)
        if isinstance(value, User):
            user = value
        client_group = Group.objects.get(name=PERMISSION_CLASSES['client'])
        if user.groups.all()[0] == client_group:
            raise ValidationError(self.message)

from datetime import datetime, timedelta
from pytz import timezone
from counter_crime.settings import TIME_ZONE

def get_year(days):
    return  days.days / 365.25

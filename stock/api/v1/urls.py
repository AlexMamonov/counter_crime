from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import (
    StockList,
    StockDetail,
)


urlpatterns = [
    url(r'^stocks/$', StockList.as_view(), name='stock-list'),
    url(r'^stocks/(?P<pk>[0-9]+)$', StockDetail.as_view(), name='stock-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)

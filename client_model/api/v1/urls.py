from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import (
    ClientModelList,
    ClientModelDetail,
)


urlpatterns = [
    url(r'^client_models/$', ClientModelList.as_view(), name='clientmodel-list'),
    url(r'^client_models/(?P<pk>[0-9]+)$', ClientModelDetail.as_view(), name='clientmodel-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)

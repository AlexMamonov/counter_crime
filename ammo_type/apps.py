from django.apps import AppConfig


class AmmoTypeConfig(AppConfig):
    name = 'ammo_type'

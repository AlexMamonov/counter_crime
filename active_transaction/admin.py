from django.contrib import admin
from .models import ActiveTransaction

class ActiveTransactionAdmin(admin.ModelAdmin):
    list_display = [field.name for field in ActiveTransaction._meta.fields]

    class Meta:
        model = ActiveTransaction

admin.site.register(ActiveTransaction, ActiveTransactionAdmin)
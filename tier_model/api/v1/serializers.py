from rest_framework import serializers
from tier_model.models import TierModel

class TierModelSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TierModel
        fields = '__all__'
from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import  (TierModelList, TierModelDetail)

urlpatterns = [
    url(r'^tiers/$', TierModelList.as_view(), name='tiermodel-list'),
    url(r'^tiers/(?P<pk>[0-9]+)$', TierModelDetail.as_view(), name='tiermodel-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
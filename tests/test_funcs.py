# runserver for create db
# delete all migrations folder in the project
# run makemigrations for each app in the backend project
# run migrate
# create super user admin , login as admin, and set token in fill_db.py
# run server

import os
import requests
import json

sever_url = "http://127.0.0.1:8000/"
client = requests.session()


def upload_data(data, url):
    for d in data:
        d = json.dumps(d)
        print("uploading: {}".format(d))
        result = client.post(url, data=d, headers=headers,
                             cookies=client.cookies)
        print(result.status_code)
        print(result.json())


def get_data(url, check_dict):
    print("getting: {}".format(url))
    result = client.get(url, headers=headers,
                        cookies=client.cookies)
    print(result.status_code)
    print(result.json())


login_url = sever_url + "rest-auth/login/"
get_clients_birthday_url = sever_url + "get_client_birthdays/"
close_work_period_url = sever_url + "close_work_period/"
otp_match_url = sever_url + "otp_match/"

user = {
    "username": "admin",
    "email": '',
    "password": "asdasd"
}

result = client.post(login_url, data=user).content.decode('utf-8')
token = json.loads(result).get('key')

headers = {
    "Authorization": "Token " + token,
    'X-CSRFToken': client.cookies['csrftoken'],
    # 'Content-Type': 'application/json',
    'charset': 'utf-8'
}

expected_json = [
    {"first_name": "Александр", "second_name": "Мамонов",
        "last_name": "Мамонов", "birthday": "1999-06-10"}
]

close_work_period_data = [
    {"pk": 1}
]

send_otp_token = [
    {
        "phone": "89601840934",
        "otp": '226377306'
    }
]

upload_data(close_work_period_data, close_work_period_url)
get_data(get_clients_birthday_url, expected_json)
# upload_data(send_otp_token, otp_match_url)
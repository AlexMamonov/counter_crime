from django.db import models
from staff.models import Staff
from completed_transaction.models import CompletedTransaction



class CompletedWorkPeriod(models.Model):
    """
    Model for storing information about completed work periods
        fields:
            result -> result of this work period
            working_time -> working hours
            staff -> array of staff that worked in this tier (ManyToMany)
            created -> date and time when the completed work period was created
            completed_transaction -> completed transactions (ManyToMany)

    """
    class Meta:
        db_table = "CompletedWorkPeriod"
        verbose_name = "Законченная смена"
        verbose_name_plural = "Законченные смены"
        permissions = (
            ("view_completed_work_period", "Can view Законченные смены"),
        )

    result = models.CharField(verbose_name="Результат", blank=True, default=None, max_length=100, null=True)
    working_time = models.TimeField(verbose_name="Время смены", blank=False, null=False)
    staff = models.ManyToManyField(Staff, verbose_name="Сотрудник",  blank=False)
    created = models.DateTimeField(verbose_name="Created", auto_now_add=True, auto_now=False)
    completed_transaction = models.ManyToManyField(CompletedTransaction, verbose_name="Завершенные транзакции", blank=False)

    def __str__(self):
        return '{} {} {}'.format(self.staff, self.result, self.created)
from django.contrib import admin
from .models import CompletedWorkPeriod

class CompletedWorkPeriodAdmin(admin.ModelAdmin):
    list_display = [field.name for field in CompletedWorkPeriod._meta.fields]

    class Meta:
        model = CompletedWorkPeriod

admin.site.register(CompletedWorkPeriod, CompletedWorkPeriodAdmin)

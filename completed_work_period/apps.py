from django.apps import AppConfig


class CompleteWorkPeriodConfig(AppConfig):
    name = 'completed_work_period'

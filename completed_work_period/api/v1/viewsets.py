from .serializers import CompletedWorkPeriodSerializer
from completed_work_period.models import CompletedWorkPeriod
from user.permissions import ExtendedModelPermissionsNonAuthReadOnly
from rest_framework.filters import SearchFilter
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import (
    generics,
    permissions
)


class CompletedWorkPeriodList(generics.ListCreateAPIView):
    queryset = CompletedWorkPeriod.objects.all()
    serializer_class = CompletedWorkPeriodSerializer

    filter_backends = (
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter,
    )

    filter_fields = '__all__'
    search_fields = '__all__'
    ordering_fields = '__all__'

    permission_classes = [
        # permissions.IsAuthenticatedOrReadOnly,
        ExtendedModelPermissionsNonAuthReadOnly
        # permissions.DjangoModelPermissionsOrAnonReadOnly,
        # ExtendedModelPermissions
    ]


class CompletedWorkPeriodDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = CompletedWorkPeriod.objects.all()
    serializer_class = CompletedWorkPeriodSerializer

    permission_classes = [
        # permissions.IsAuthenticated,
        # permissions.IsAuthenticatedOrReadOnly,
        ExtendedModelPermissionsNonAuthReadOnly
        # ExtendedModelPermissions
    ]

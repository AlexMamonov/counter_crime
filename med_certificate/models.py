from django.db import models
from django.db.models.signals import post_delete
from django.core.files.storage import FileSystemStorage
from utils.rename_image import rename_med_sertificate_images
from client_model.models import  ClientModel
from counter_crime.settings import IMAGE_STORAGE

class MedCertificate(models.Model):
    """
    Model for storing information about medical certificates
        fields:
            image -> image of med certificate
            client -> client (ClientModel)
            number -> medical certificate number
            issuer_date -> date of issue of the medical certificate
            created -> date and time of creation
        additional funcs:
            delete_image: delete image of the medical certificate

    """
    class Meta:
        db_table = 'MedCertificate'
        verbose_name = 'Медицинская справка'
        verbose_name_plural = 'Медицинские справки'
        permissions = (
            ('view_med_certificate', 'Can view Медицинские справки'),
        )

    image = models.ImageField(verbose_name="Фото мед справки", storage=IMAGE_STORAGE, max_length=500, blank=True, upload_to=rename_med_sertificate_images)
    client = models.ForeignKey(ClientModel, on_delete=models.CASCADE,  blank=False)
    number = models.SmallIntegerField(verbose_name="Номер справки", blank=False, null=False)
    issuer_date = models.DateField(verbose_name="Дата выдачи", blank=False, null=False)
    created = models.DateTimeField(verbose_name='Created', auto_now_add=True, auto_now=False)

    def __str__(self):
        return '{USER} {NUMBER} {IMAGE}'.format(
            USER=self.user,
            NUMBER=self.number,
            IMAGE=self.pk,
            ISSUER_DATE=self.issuer_date,
        )

    @staticmethod
    def detete_image(sender, instance, *args, **kwargs):
        if instance.image:
            print(instance.image.path)
            if os.path.isfile(instance.image.path):
                os.remove(instance.image.path)

post_delete.connect(sender=MedCertificate, receiver=MedCertificate.detete_image)

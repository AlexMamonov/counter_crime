from django.apps import AppConfig


class MedCertificateConfig(AppConfig):
    name = 'med_certificate'

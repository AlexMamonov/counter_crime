from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import (
    StaffPositionList,
    StaffPositionDetail,
)


urlpatterns = [
    url(r'^staff_positions/$', StaffPositionList.as_view(), name='staffposition-list'),
    url(r'^staff_positions/(?P<pk>[0-9]+)$', StaffPositionDetail.as_view(), name='staffposition-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)

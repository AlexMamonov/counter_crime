from django.contrib import admin
from .models import RegistrationPlace

class RegistrationPlaceAdmin(admin.ModelAdmin):
    list_display = [field.name for field in RegistrationPlace._meta.fields]

    class Meta:
        model = RegistrationPlace

admin.site.register(RegistrationPlace, RegistrationPlaceAdmin)
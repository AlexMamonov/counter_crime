from django.apps import AppConfig


class RegistrationPlaceConfig(AppConfig):
    name = 'registration_place'

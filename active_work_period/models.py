from django.db import models
from staff.models import Staff
from django.db.models.signals import post_save
from completed_transaction.models import CompletedTransaction
from active_transaction.models import ActiveTransaction

class ActiveWorkPeriod(models.Model):
    """
    Model for storing active shift
        fields:
            staff -> array of staff that works on this shift (ManyToMany)
            created -> date and time of the start of the shift
            active_transaction -> active transactions (ManyToMany)
            completed_transaction -> completed transactions (ManyToMany)

    """
    class Meta:
        db_table = "ActiveWorkPeriod"
        verbose_name = "Активная смена"
        verbose_name_plural = "Активные смены"
        permissions = (
            ("view_active_work_period", "Can view Активные смены"),
        )

    staff = models.ManyToManyField(Staff, verbose_name="Сотрудники", blank=False)
    created = models.DateTimeField(verbose_name="Время начала смены", auto_now_add=True, auto_now=False)
    active_transaction = models.ManyToManyField(ActiveTransaction, verbose_name="Активные транзакции", blank=True)
    completed_transaction = models.ManyToManyField(CompletedTransaction, verbose_name="Завершенные транзакции", blank=True)

    def __str__(self):
        return "{STAFF} {CREATED}".format(
            STAFF=self.staff,
            CREATED=self.created,
        )
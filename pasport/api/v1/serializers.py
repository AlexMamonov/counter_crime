from rest_framework import serializers
from pasport.models import Pasport

class PasportSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Pasport
        fields = '__all__'

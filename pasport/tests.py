from django.test import TestCase
from pasport.models import Pasport
import datetime
# Create your tests here.


class PassportTestCase(TestCase):
    def setUp(self):
        Pasport.objects.create(
            name="NAME", created=datetime.datetime.now(), updated=datetime.datetime.now())
        Pasport.objects.create(
            name="SURNAME", created=datetime.datetime.now(), updated=datetime.datetime.now())

    def test_passport_fields(self):
        name = Pasport.objects.get(pk=1)
        assert str(name) == "NAME"
        name = Pasport.objects.get(pk=2)
        assert str(name) == "SURNAME"

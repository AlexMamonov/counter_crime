from django.contrib import admin
from .models import Pasport

class PasportAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Pasport._meta.fields]

    class Meta:
        model = Pasport

admin.site.register(Pasport, PasportAdmin)

from .serializers import StaffSerializer
from staff.models import Staff
from user.permissions import ExtendedModelPermissionsNonAuthReadOnly
from rest_framework.filters import SearchFilter
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import (
    generics,
    permissions
)
from rest_framework.decorators import api_view
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from django.core.serializers import serialize
from rest_framework.response import Response
from rest_framework import status
import json

class StaffList(generics.ListCreateAPIView):
    queryset = Staff.objects.all()
    serializer_class = StaffSerializer

    filter_backends = (
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter,
    )

    # filter_fields = '__all__'
    search_fields = '__all__'
    ordering_fields = '__all__'

    permission_classes = [
        # permissions.IsAuthenticatedOrReadOnly,
        ExtendedModelPermissionsNonAuthReadOnly
        # permissions.DjangoModelPermissionsOrAnonReadOnly,
        # ExtendedModelPermissions
    ]


class StaffDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Staff.objects.all()
    serializer_class = StaffSerializer

    permission_classes = [
        # permissions.IsAuthenticated,
        # permissions.IsAuthenticatedOrReadOnly,
        ExtendedModelPermissionsNonAuthReadOnly
        # ExtendedModelPermissions
    ]

    def post(self, request, *args, **kwargs):
        file_serializer = StaffSerializer(data=request.data)

        if file_serializer.is_valid():
            file_serializer.save()
            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
def get_staff_birthdays(request, format=None):
    if request.method == 'GET':
        staffs = Staff.objects.all()
        staff_birthdays = []
        for staff in staffs:
            data = serialize('json',[staff.user])
            data = json.loads(data)[0]['fields']
            staff_birthdays.append(
                {
                    "first_name": data['first_name'],
                    "second_name": data['second_name'],
                    "last_name": data['last_name'],
                    "birthday": data['birthday'],
                }
            )
        return Response(data=staff_birthdays,  status=status.HTTP_200_OK,)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)
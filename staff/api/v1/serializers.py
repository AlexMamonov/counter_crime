from rest_framework import serializers
from staff.models import Staff

class StaffSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Staff
        fields = '__all__'

    image = serializers.ImageField(required=False,max_length=None, use_url=True)
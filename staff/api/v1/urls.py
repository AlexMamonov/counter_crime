from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import  (StaffList, StaffDetail)

urlpatterns = [
    url(r'^staffs/$', StaffList.as_view(), name='staff-list'),
    url(r'^staffs/(?P<pk>[0-9]+)$', StaffDetail.as_view(), name='staff-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
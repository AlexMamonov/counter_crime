from django.apps import AppConfig


class WeaponTypeConfig(AppConfig):
    name = 'weapon_type'

from django.contrib import admin
from .models import WeaponType

class WeaponTypeAdmin(admin.ModelAdmin):
    list_display = [field.name for field in WeaponType._meta.fields]

    class Meta:
        model = WeaponType

admin.site.register(WeaponType, WeaponTypeAdmin)
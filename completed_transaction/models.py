from django.db import models
from client_model.models import ClientModel
from weapon.models import Weapon


class CompletedTransaction(models.Model):
    """
    Model for storing completed transactions
        fields:
            result -> result of this transaction
            client -> client (ClientModel)
            weapon -> type of weapon (weapon)
            created -> date and time of creation of the completed transaction

    """
    class Meta:
        db_table = "CompletedTransactions"
        verbose_name = "Законченная транзакция"
        verbose_name_plural = "Законченные транзакции"
        permissions = (
            ("view_completed_transaction", "Can view Законченные транзакции"),
        )

    result = models.CharField(
        verbose_name="Результат", blank=True, default=None, max_length=100, null=True)
    client = models.ForeignKey(ClientModel, verbose_name="Клиент",
                                  on_delete=models.CASCADE, blank=False, null=False)
    weapon = models.ForeignKey(Weapon, verbose_name="Оружие",
                                  on_delete=models.CASCADE, blank=False, null=False)
    created = models.DateTimeField(
        verbose_name="Created", auto_now_add=True, auto_now=False)

    def __str__(self):
        return '{} {} {} {}'.format(self.client, self.weapon, self.result, self.created)
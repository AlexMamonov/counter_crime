from django.apps import AppConfig


class CompletedTransactionConfig(AppConfig):
    name = 'completed_transaction'

from utils.helpers import check_phone_number
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.hashers import check_password
from user.models import User


class PhoneNumberLoginBacked(ModelBackend):

    def authenticate(self, request, phone_number=None, password=None):
        # Check the phone_number/password and return a user.
        phone_number_is_valid = check_phone_number(phone_number)
        if phone_number_is_valid:
            try:
                user = User.objects.get(phone_number=phone_number)
                if check_password(password, user.password):
                    return user
            except User.DoesNotExist:
                msg = _('No such User')
                raise ValidationError(msg)
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import (
    UserList,
    UserDetail,
    GroupList,
    GroupDetail
)


urlpatterns = [
    url(r'^users/$', UserList.as_view(), name='user-list'),
    url(r'^users/(?P<pk>[0-9]+)$', UserDetail.as_view(), name='user-detail'),

    url(r'^groups/$', GroupList.as_view(), name='group-list'),
    url(r'^groups/(?P<pk>[0-9]+)$', GroupDetail.as_view(), name='group-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)

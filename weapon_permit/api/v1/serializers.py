from rest_framework import serializers
from weapon_permit.models import WeaponPermit

class WeaponPermitSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = WeaponPermit
        fields = '__all__'

    image = serializers.ImageField(required=False,max_length=None, use_url=True)
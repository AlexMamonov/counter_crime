from django.db import models

class Target(models.Model):
    """
    Model for storing information about targets
        fields:
            result -> result

    """
    class Meta:
        db_table = "TargetModel"
        verbose_name = "Мишень"
        verbose_name_plural = "Мишени"
        permissions = (
            ("view_target_model", "Can view Мишени"),
        )

    result = models.CharField(verbose_name="Результат", max_length=100, blank=False, null=False)


    def __str__(self):
        return '{RESULT}'.format(
            RESULT = self.result,
        )
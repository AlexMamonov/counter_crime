from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import  (TargetModelList, TargetModelDetail)

urlpatterns = [
    url(r'^targets/$', TargetModelList.as_view(), name='target-list'),
    url(r'^targets/(?P<pk>[0-9]+)$', TargetModelDetail.as_view(), name='target-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
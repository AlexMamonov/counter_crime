from rest_framework import serializers
from target_model.models import Target

class TargetModelSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Target
        fields = '__all__'